## [1.0.1](https://gitlab.com/just-ci/tests/versioning/compare/v1.0.0...v1.0.1) (2022-09-01)


### Bug Fixes

* test there ([ce46b9a](https://gitlab.com/just-ci/tests/versioning/commit/ce46b9a58b81bc69c3edf511ebf0ef9e8496e52e))

# 1.0.0 (2022-09-01)


### Bug Fixes

* readme ([ec44a45](https://gitlab.com/just-ci/tests/versioning/commit/ec44a45b96ae1ea660bf03b86df65ab3f8285504))
* tbump me ([bed56aa](https://gitlab.com/just-ci/tests/versioning/commit/bed56aaeeac8155aa03d3c0507967fa754083957))
